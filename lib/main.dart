import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kised_app/routes.dart';

void main() =>
    {runApp(GetMaterialApp(initialRoute: "/SplashScreen", getPages: routes))};
