import 'package:get/get.dart';
import 'package:kised_app/screens/splash_screen.dart';

final List<GetPage> routes = [
  GetPage<SplashScreen>(
      name: "/SplashScreen", page: () => const SplashScreen()),
];
