import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kised_app/controllers/device_info_controller.dart';
import 'package:kised_app/controllers/video_state_controller.dart';
import 'package:kised_app/widgets/avatar_video_player.dart';
import 'package:kised_app/widgets/background_stopped_image.dart';
import 'package:kised_app/widgets/menus/menu.dart';
import 'package:kised_app/widgets/setting_btn.dart';
import '../widgets/current_time.dart';
import '../controllers/port_state_controller.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late Image settingImage;
  late List<Image> scheduleImage;
  late Image allScheduleImage;

  @override
  void initState() {
    Get.put(PortStateController()).getPorts();
    Get.put(DeviceInfoController()).onInit();
    settingImage = Image.asset("assets/img/kiosk-background.png");
    scheduleImage = [Image.asset("assets/img/002.png"),Image.asset("assets/img/003.png"),Image.asset("assets/img/004.png")];
    allScheduleImage = Image.asset("assets/img/img_timetable.jpg");
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    precacheImage(settingImage.image, context);
    precacheImage(scheduleImage[0].image, context);
    precacheImage(scheduleImage[1].image, context);
    precacheImage(scheduleImage[2].image, context);
    precacheImage(allScheduleImage.image, context);
  }

  @override
  void dispose() {
    super.dispose();
    Get.put(PortStateController()).connectTo(null);
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Center(
          child: Stack(
            children: [
              BackgroundStoppedImage(),
              AvatarVideoPlayer(),
              Positioned(
                top: height * 0.02083,  // 80
                left: width * 0.03704,  // 80
                child: SizedBox(
                  height: height * 0.088802083, // 341
                  width: width * 0.22222, // 480
                  child: SvgPicture.asset(
                    "assets/img/kisedLogo.svg",
                    color: Colors.white,
                  ),
                 ),
              ),
              Positioned(
                right: width * 0.03704,
                top: height * 0.0174479167,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: const [
                    CurrentTime(),
                  ],
                ),
              ),
              GetBuilder<PortStateController>(
                  init: PortStateController(),
                  builder: (controller) => controller.isDetected?MainMenu():Container() // 근접센서 !
              ),
              SettingBtn(settingImage: settingImage,),
              Positioned(
                right: height * 0.02083,  // 80
                bottom: width * 0.03704,  // 80
                child: SizedBox(
                  height: height * 0.02083, // 80
                  width: width * 0.18519, // 400
                  child: SvgPicture.asset(
                    "assets/img/stpLogo.svg",
                    color: Colors.white,
                  ),
                ),
              ),

              // Positioned(
              //   right: width * 0.1,
              //   top: height * 0.1 ,
              //   child: ElevatedButton(
              //     child: Text('Welcome', style: TextStyle(fontSize: width*0.05),),
              //     onPressed: (){
              //       Get.put(PortStateController()).setIsDetected(false);
              //           Get.put(VideoStateController())
              //               .changeUrl("assets/video/opening.mp4",isChange: true, changingUrl: ["assets/video/018.mp4","assets/video/wait_left.mp4"]);
              //     },
              //   ),
              // ),

              // test button
              // Positioned(
              //   right: width * 0.3,
              //     bottom: height*0.05,
              //     child: ElevatedButton(
              //       child: Text('YES', style: TextStyle(fontSize: width*0.05),),
              //       onPressed: (){
              //         Get.put(PortStateController()).testFunction('yes');
              //       },
              //     )
              // ),
              // Positioned(
              //     right: width * 0.2,
              //     bottom: height*0.05,
              //     child: ElevatedButton(
              //       child: Text('NO', style: TextStyle(fontSize: width*0.05),),
              //       onPressed: (){
              //         Get.put(PortStateController()).testFunction('no');
              //       },
              //     )
              // )
            ],
          ),
        ),
      ),
    );
  }
}
