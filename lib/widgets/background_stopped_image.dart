import 'package:flutter/material.dart';
import 'package:kised_app/controllers/video_state_controller.dart';
import 'package:get/get.dart';

class BackgroundStoppedImage extends StatelessWidget {
  const BackgroundStoppedImage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GetBuilder<VideoStateController>(init: VideoStateController(), builder: (controller){
      return Container(
        height: size.height,
        width: size.width,
        child: Image.asset(
          controller.isCenterUrl()?'assets/img/background_center.png':'assets/img/background_left.png',
          fit: BoxFit.fill,
        ),
      );
    });
  }
}