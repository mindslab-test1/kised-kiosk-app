import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kised_app/controllers/chat_box_controller.dart';
import 'package:kised_app/repository/repository.dart';
import 'package:kised_app/widgets/all_schedule.dart';
import 'package:kised_app/widgets/first_button.dart';

class ChatMenu extends StatelessWidget {
  const ChatMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ChatBoxController chatBoxController = Get.put(ChatBoxController());
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    return Container(
      padding: EdgeInsets.only(right: width * 0.03704),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                SizedBox(
                  height: height * 0.2401,
                ),
                Container(
                  width: width * 0.4134259,
                  child: Stack(
                    children: [
                      SizedBox(
                        width: width * 0.39815,
                        child: GetBuilder<ChatBoxController>(
                          builder: (controller) {
                            List<Widget>? listWidget;
                            if (chatBoxController.chats != []) {
                              listWidget = chatBoxController.chats
                                  .map((e) => SideButton(text: e))
                                  .toList();
                            } else {
                              listWidget = null;
                            }

                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  width: width * 0.39815,
                                  child: Text(
                                    chatBoxController.chatTitle,
                                    style: TextStyle(
                                      fontFamily: 'notoSansKR',
                                      fontWeight: FontWeight.w400,
                                      color: Colors.white,
                                      fontSize: width * 0.02407,
                                      // 52
                                      letterSpacing: -1.3,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: height * 0.00625, // 24
                                ),
                                (chatBoxController.imageUrl != '')
                                    ? Container(
                                        margin: EdgeInsets.only(
                                            top: height * 0.01667), // 64
                                        width: width * 0.39815, // 860
                                        child: (ClipRRect(
                                          borderRadius: BorderRadius.circular(
                                              width * 0.007407), // 16
                                          child: BackdropFilter(
                                            filter: ImageFilter.blur(
                                              sigmaX: width * 0.00833, // 18
                                              sigmaY: width * 0.00833, // 00833
                                            ),
                                            child: Center(
                                              child: Container(
                                                padding: EdgeInsets.all(
                                                    width * 0.02963), // 64
                                                width: width * 0.39815, // 860
                                                decoration: BoxDecoration(
                                                  color: Colors.black
                                                      .withOpacity(0.4),
                                                ),
                                                child: Center(
                                                    child: SizedBox(
                                                  width: width * 0.33889,
                                                  // 732
                                                  child: Image.asset(
                                                    chatBoxController.imageUrl,
                                                    fit: BoxFit.contain,
                                                  ),
                                                )),
                                              ),
                                            ),
                                          ),
                                        )),
                                      )
                                    : Container(),
                                if (listWidget != null)
                                  Column(
                                    children: [
                                      Wrap(children: listWidget),
                                    ],
                                  ),
                                Container(
                                  width: width * 0.39815,
                                  child: FirstButton(
                                    height: height,
                                    width: width,
                                  ),
                                )
                              ],
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class SideButton extends StatefulWidget {
  final String text;

  const SideButton({Key? key, required this.text}) : super(key: key);

  @override
  _SideButtonState createState() => _SideButtonState();
}

class _SideButtonState extends State<SideButton> {
  Color buttonColor = Colors.black.withOpacity(0.4);

  @override
  Widget build(BuildContext context) {
    ChatBoxController chatBoxController = Get.put(ChatBoxController());
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    return GetBuilder<ChatBoxController>(builder: (controller) {
      return Container(
        margin: EdgeInsets.only(top: height * 0.014167),
        // 40
        child: GestureDetector(
            onTapCancel: () {
              setState(() {
                buttonColor = Colors.black.withOpacity(0.4);
              });
            },
            onTapDown: (TapDownDetails t) {
              setState(() {
                buttonColor = Colors.black.withOpacity(0.7);
              });
            },
            onTapUp: (TapUpDetails tapUpDetails) async {
              setState(() {
                buttonColor = Colors.black.withOpacity(0.4);
              });
              if (chatBoxController.isRenewed) {
                chatBoxController.updateRenew(false);
                if (widget.text == "전체 일정") {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const AllSchedule()),
                  );
                }
                Repository.sendTextReq(widget.text);
              }
            },
            child: ClipRRect(
              borderRadius: BorderRadius.circular(width * 0.007407),
              child: BackdropFilter(
                filter: ImageFilter.blur(
                  sigmaX: width * 0.00462963,
                  sigmaY: width * 0.00462963,
                ),
                child: Container(
                  padding: EdgeInsets.fromLTRB(
                      width * 0.02963, // 64
                      height * 0.0117875,
                      0,
                      height * 0.0117875),
                  // height: height * 0.04167,
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: width * 0.000462963,
                      color: Color.fromRGBO(0, 0, 0, 0.25),
                    ),
                    color: buttonColor,
                  ),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: width * 0.02963,
                        height: width * 0.02963,
                        child: Image.asset(
                          'assets/img/ico_rocket_64px.png',
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(width: width * 0.02222), // 48
                      Flexible(
                        child: Container(
                          child: Text(
                            widget.text,
                            // softWrap: true,
                            textAlign: TextAlign.left,
                            overflow: TextOverflow.visible,
                            // overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              fontFamily: 'naumSquare',
                              fontWeight: FontWeight.w400,
                              color: Colors.white,
                              fontSize: width * 0.02963,
                              height: 1.1,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )),
      );
    });
  }
}
